fetch('https://api.mockaroo.com/api/2e9de360?count=10&key=ae412dc0')
    .then(response => response.json())
    .then(data => {
        const tableBody = document.querySelector(".table__body");
        for (let i = 0; i < data.length; i++) {
            const personData = data[i];

            const addRow = document.createElement("tr");
            if (i % 2 == 0) {
                addRow.className = "table__body__row--odd"
            } else {
                addRow.className = "table__body__row--even"
            }

            const newImgCell = document.createElement("td");
            newImgCell.className = "table__body__cell";
            const newImg = document.createElement("img");
            newImg.setAttribute("src", personData.avatar);
            newImg.setAttribute("alt", "profile");
            newImg.className = "image";
            newImgCell.appendChild(newImg);
            addRow.appendChild(newImgCell);

            const newNameCell = document.createElement("td");
            newNameCell.className = "table__body__cell";
            const newNameTextField = document.createTextNode(personData.name);
            newNameCell.appendChild(newNameTextField);
            addRow.appendChild(newNameCell);

            const newLocationCell = document.createElement("td");
            newNameCell.className = "table__body__cell";
            const newLocationTextField = document.createTextNode(personData.location);
            newLocationCell.appendChild(newLocationTextField);
            addRow.appendChild(newLocationCell);

            const newEmailCell = document.createElement("td");
            newEmailCell.className = "table__body__cell";
            const newEmailTextField = document.createTextNode(personData.email);
            newEmailCell.appendChild(newEmailTextField);
            addRow.appendChild(newEmailCell);

            const newPhoneCell = document.createElement("td");
            newPhoneCell.className = "table__body__cell";
            const newPhoneTextFeild = document.createTextNode(personData.phone);
            newPhoneCell.appendChild(newPhoneTextFeild);
            addRow.appendChild(newPhoneCell);


            tableBody.appendChild(addRow)
        }
    }
)
